# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, ... }:
let
  prj = "gics";
in {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./packages.nix
      ./loginctl-linger.nix
      ./startchrony.nix
      ./fixlockdir.nix
    ];

  boot.kernelPackages = pkgs.linuxPackages;
  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda";
  # Enable serial console for GRUB
  boot.loader.grub.extraConfig = "serial --speed=115200 --unit=0; terminal_input --append serial; terminal_output --append serial";

  # Enable serial console for Linux
  boot.kernelParams = ["console=tty1" "console=ttyS0,115200n8"];

  # Sysctl settings
  boot.kernel.sysctl."kernel.panic" = 30;

  networking.hostName = "${prj}-1"; # Define your hostname.
  # Static address for second interface
  networking.interfaces.ens36.ip4 = [
    { address = "10.13.1.2"; prefixLength = 24; }
  ];
  networking.firewall.enable = false;

  # Run a DHCP server on the second interface
  # services.dhcpd.enable = true;
  # services.dhcpd.interfaces = "ens36";
  # services.dhcpd.extraConfig = ''
  #   subnet 10.13.1.0 netmask 255.255.255.0
  #      {
  #        range 10.13.1.100 10.13.1.200;
  #        default-lease-time 86400;
  #        max-lease-time 2592000;
  #      }
  # '';
  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "UTC";
  services.ntp.enable = true;
  networking.timeServers = [
    "10.13.1.10"
    "time1.u.washington.edu"
  ];

  # ICEX hosts
  networking.extraHosts = ''
  10.13.1.10 icex-ntp
  10.13.1.1 icex-gw
  10.13.1.3 dirs
  10.13.1.4 atrs2
  '';

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  services.atd.enable = true;
  services.atd.allowEveryone = true;

  # Redis
  services.redis.enable = false;

  # Fix permissions on /var/lock for Kermit
  services.fixlockdir.enable = true;

  # Docker
  virtualisation.docker.enable = true;
  virtualisation.docker.enableOnBoot = true;

  # SMB file sharing
  services.samba.enable = true;
  services.samba.syncPasswordsByPam = true;
  services.samba.shares = {
    data = {
      path = "/data/ICEX";
      "read only" = "yes";
      browseable = "yes";
      "write list" = "sysop";
      "create mask" = 0644;
      "directory mask" = 0755;
    };
  };
  services.samba.extraConfig = ''
    workgroup = APLIS
    hosts allow = 127.0.0.1 10.208.78.0/24 10.13.0.0/16
  '';

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.sysop = {
    isNormalUser = true;
    uid = 1000;
    description = "System Operator";
    extraGroups = ["wheel" "users" "dialout" "docker"];
    linger = true;
  };

  security.sudo = {
    enable = true;
    wheelNeedsPassword = false;
  };

  nix.trustedUsers = ["root" "sysop"];

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "17.09";

}
