{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    wget
    chrony
    which
    emacs25-nox
    vim
    tmux
    fping
    gitAndTools.gitFull
    socat
    gcc
    gnumake
    pciutils
    kermit
    attr
    samba
    lsof
    pkgconfig
    hdparm
  ];

}
