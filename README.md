# NixOS Configuration for GICS

Custom NixOS configuration for the GICS computer.

## Prerequisites

See the [NixOS Manual](https://nixos.org/nixos/manual/) for the basic
installation instructions.

+ A minimal NixOS installation with `git` installed

## Installation

``` shellsession
mv /etc/nixos /etc/nixos.dist
git clone https://bitbucket.org/uwaploe/gics-nixos-config.git /etc/nixos
cp /etc/nixos.dist/hardware-configuration.nix /etc/nixos/
ln -sr /etc/nixos/config.nix /etc/nixos/configuration.nix
nixos-rebuild switch
```
