{ config, lib, pkgs, ... }:
let
  cfg = config.services.fixlockdir;
in {
  options = {
    services.fixlockdir = {
      enable = lib.mkOption {
        type = lib.types.bool;
        default = false;
        description = ''
          Whether to enable `fixlockdir' service.
        '';
      };
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.services.fixlockdir = {
      description = "Fix permissions on /run/lock";
      wantedBy = [ "multi-user.target" ];
      path = [ pkgs.coreutils ];
      serviceConfig = {
        Type = "oneshot";
        ExecStart = ''
          ${pkgs.coreutils}/bin/chmod 1777 /run/lock
        '';
      };
    };
  };
}
